#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import simplertp
import random


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    def handle(self):
        """Esta definicion controla el proceso."""
        # Escribe dirección y puerto del cliente (de tupla client_address)
        mensaje = self.rfile.read().decode("utf-8")
        metodo = mensaje.split()[0]
        print(metodo, "recibido")
        if metodo == "INVITE":
            try:
                line = mensaje.split("\r\n")[0]
                cuerpo_SDP = mensaje.split("\r\n")[1:]
                UA = line.split()[1].split(":")[1].split("@")[0]
                IP_UA = line.split()[1].split(":")[1].split("@")[1]
                SDP = "Content-Type: application/SDP\r\n\r\nv=0\r\n"
                SDP += "o= " + UA + " " + IP_UA + "\r\n"
                SDP += "s = noelia sesion\r\nt=0\r\n"
                SDP += "m=audio 34543 RTP\r\n\r\n"
                m = "SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 Ringing\r\n\r\n"
                m += "SIP/2.0 200 OK\r\n\r\n"
                self.wfile.write(bytes(m+SDP, "utf-8") + b"\r\n")
            except IndexError:
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n")
        elif metodo == "BYE":
            self.wfile.write(b"SIP/2.0 200 OK\r\n")
        elif metodo == "ACK":
            ALEAT = random.randint(0, 99)
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0,
                                  marker=0, ssrc=ALEAT)
            audio = simplertp.RtpPayloadMp3(fichero_audio)
            simplertp.send_rtp_packet(RTP_header, audio, "127.0.0.1", 23032)
        else:
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n")


if __name__ == "__main__":
    try:
        if len(sys.argv) != 4:
            sys.exit("Usage: python3 server.py IP PORT fichero_audio")
        IP_UA = sys.argv[1]
        PORT_UA = int(sys.argv[2])
        fichero_audio = sys.argv[3]

    except ValueError:
        sys.exit("Usage: python3 server.py IP PORT fichero_audio")

    # Creamos servidor de eco y escuchamos
    serv = socketserver.UDPServer((IP_UA, PORT_UA), EchoHandler)
    print("Listening...")
    serv.serve_forever()()
