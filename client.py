#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""

import socket
import sys


# Cliente UDP simple.

# Dirección IP del servidor.
if len(sys.argv) == 3:
    METODO = sys.argv[1]
    UA = sys.argv[2]
else:
    sys.exit("Usage: python3 client.py METODO UA@IPUA:PORTsip")

# Contenido que vamos a enviar
try:
    nombre_UA = UA.split("@")[0]
    IP_UA = UA.split("@")[1].split(":")[0]
    PORT_UA = int(UA.split("@")[1].split(":")[1])
except ValueError:
    sys.exit("Usage: python3 client.py METODO UA@IPUA:PORTsip")

if METODO.upper() == 'INVITE':
    info = "INVITE sip:" + nombre_UA + "@" + IP_UA + " SIP/2.0\r\n"
    ACK = "ACK sip:" + nombre_UA + "@" + IP_UA + " SIP/2.0\r\n"

elif METODO.upper() == 'BYE':
    info = "BYE sip:" + nombre_UA + "@" + IP_UA + " SIP/2.0\r\n"

else:
    info = METODO.upper() + " sip:" + nombre_UA + "@" + IP_UA + " SIP/2.0\r\n"

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((IP_UA, PORT_UA))

    print("Enviando: " + info)
    my_socket.send(bytes(info, "utf-8") + b"\r\n")
    data = my_socket.recv(1024).decode("utf-8")

    print('Recibido -- ', data)
    if "100 Trying" in data or "180 Ringing" in data or "200 OK" in data:
        my_socket.send(bytes(ACK, "utf-8") + b"\r\n")
    print("Terminando socket...")

print("Fin.")
